import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GlobalProvider } from '../../providers/global/global';
import { HomePage } from '../home/home';
import { Usuario } from '../../classes/usuario/usuario';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public formLogin: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public formBuilder: FormBuilder,
    public globalProvider: GlobalProvider, ) {
    globalProvider.cambiarTitulo("Fundación UPB");
    this.formLogin = formBuilder.group({
      email: ['', Validators.email],
      password: ['', Validators.required]
    });
  }

  ionViewDidLoad() {
  }

  login() {
    const formValue = this.formLogin.value;
    let usuario = new Usuario();
    usuario.email = formValue.email;
    usuario.password = formValue.password;
    let res = this.globalProvider.loguearUsuario(usuario);
    if (res.success) {
      this.cambiarPagina();
    } else {
      this.globalProvider.showToast(res.message);
    }
  }


  cambiarPagina() {
    this.navCtrl.setRoot(HomePage);
  }

}
