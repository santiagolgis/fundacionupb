import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  public profile: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.profile = {
      nombre: "Juan David Roa Piñeros",
      horas: "256",
      foto: "../../assets/imgs/profile.jpg"
    }
  }

  ionViewDidLoad() {

  }

}
