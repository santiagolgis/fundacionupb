import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { NewsPage } from '../news/news';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public lstNoticias: Array<{ title: string, body: string }>;

  constructor(public navCtrl: NavController, public globalProvider: GlobalProvider) {
    globalProvider.cambiarTitulo("Noticias");
    this.lstNoticias = [
      { title: 'Becado es pre-seleccionado para ganar la Medalla al Mérito Pedro Pascasio Martínez', body: "Pedro Pascasio Martínez es más conocido como el “niño soldado”, este participó en la campaña libertadora cuando tenía la edad de 12 años. En honor a él se creó la Medalla al Mérito Pedro Pascasio Martínez de Ética Republicana por medio de la Ley 668 del 2001, con esta ley se busca otorgar a jóvenes menores de 25 años un reconocimiento por lograr a través de iniciativas individuales o colectivas, la recuperación de los valores éticos ciudadanos." },
      { title: 'Intercambio en universidades de Colombia, oportunidad con Sígueme', body: "Los estudiantes de pregrado de la UPB tienen a su disposición la posibilidad de cursar alguno de sus semestres académicos en otra universidad del país gracias al programa de movilidad denominado Convenio Sígueme." },
      { title: 'Especialización Enseñanza del Inglés a la vanguardia con tendencias internacionales', body: "El Programa de Especialización en Enseñanza del Inglés, de la Universidad Pontifica Bolivariana Seccional Montería, recibió la resolución de renovación y modificación de su registro calificado mediante la Resolución número 09781 del 18 de junio de 2018, por el término de siete años. La duración estimada del programa es de dos semestres y cuenta con 24 créditos académicos." },
      { title: 'Científico santandereano que trabaja para la NASA visitó la UPB', body: "Oriundo de Macaravita, Santander, el Dr Edbertho Leal-Quirós ha alcanzado importantes logros científicos que ha contribuido al desarrollo de avances importantes que podrían llegar a cambiar el mundo que se conoce actualmente." },
      { title: 'La Ingeniería: pieza clave para el desarrollo social y educativo del país', body: "Con el título “Gestión, calidad y desarrollo en las facultades de ingeniería”, el EIEI ACOFI 2018 se desarrolló como un evento académico, en el que decanos, directivos académicos y administrativos, profesores y estudiantes de ingeniería, interactuaron con representantes del sector productivo, entidades del Estado, gremios y miembros de  la sociedad en general,  para estudiar, analizar, debatir y reflexionar sobre cómo las acciones en gestión y aseguramiento de la calidad,  promueven el desarrollo y la construcción permanente de la excelencia en ingeniería, para el logro de sus objetivos académicos en beneficio de la sociedad." },
    ];
  }

  public mostrarDetalle(noticia: any) {
    this.navCtrl.setRoot(NewsPage, { noticia: noticia });
  }

}
