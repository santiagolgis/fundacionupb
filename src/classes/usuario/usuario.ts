export class Usuario{
    id: string;
    token: string;
    expiration: string;
    email:string;
    password: string;
    name: string;
}