import { Component } from '@angular/core';
import { GlobalProvider } from '../../providers/global/global';
import { Events, AlertController, App } from 'ionic-angular';
import { Usuario } from '../../classes/usuario/usuario';
import { LoginPage } from '../../pages/login/login';

/**
 * Generated class for the CustomHeaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'custom-header',
  templateUrl: 'custom-header.html'
})
export class CustomHeaderComponent {

  public title: string;
  public currentUser: Usuario;

  constructor(private globalProvider: GlobalProvider, public app: App, public events: Events, public alertCtrl: AlertController
  ) {
    events.subscribe('titulo:cambiado', (titulo) => {
      this.title = titulo;
    });
    this.currentUser = null;
    events.subscribe('usuario:cambiado', (usuario) => {
      this.currentUser = usuario;
    });
    this.globalProvider.observableUser.subscribe(Usuario => {
      this.currentUser = Usuario;

    });
  }


  public cerrarSesion() {
    localStorage.removeItem("Usuario");
    this.globalProvider.currentUser = null;
    this.globalProvider.usuarioChange();
    this.app.getRootNav().push(LoginPage);
  }

  public showConfirm() {
    const confirm = this.alertCtrl.create({
      title: 'Alerta!',
      message: '¿Está seguro de cerrar la sesión?',
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            this.cerrarSesion();
          }
        },
        {
          text: 'Cancelar',
          handler: () => {

          }
        }
      ]
    });
    confirm.present();
  }
}
