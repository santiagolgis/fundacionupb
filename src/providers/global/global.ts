import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastController, Events } from 'ionic-angular';
import { Usuario } from '../../classes/usuario/usuario';
import { BehaviorSubject } from 'rxjs';

/*
  Generated class for the GlobalProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GlobalProvider {

  public title: string = "";

  public lstUsuarios: Usuario[];
  public currentUser: Usuario;
  public observableUser: BehaviorSubject<Usuario>;


  constructor(public http: HttpClient, public toastCtrl: ToastController, public events: Events) {
    this.currentUser = JSON.parse(localStorage.getItem("Usuario")) == undefined ? null : JSON.parse(localStorage.getItem("Usuario")) as Usuario;
    this.observableUser = new BehaviorSubject<Usuario>(this.currentUser);
    this.usuarioChange();
    this.lstUsuarios = [];
    let usuario: Usuario = new Usuario();
    usuario.email = "villacobadrian@gmail.com";
    usuario.name = "Adrian Jose Villacob Martinez ";
    usuario.password = "12345";
    this.lstUsuarios.push(usuario);
    let usuario2: Usuario = new Usuario();
    usuario2.email = "claudiaparra087@gmail.com ";
    usuario2.name = "Claudia Patricia Parra Bejarano";
    usuario2.password = "12345";
    this.lstUsuarios.push(usuario2);
    let usuario3: Usuario = new Usuario();
    usuario3.email = "juandaroa02@gmail.com ";
    usuario3.name = "Juan David Roa Piñeros";
    usuario3.password = "12345";
    this.lstUsuarios.push(usuario3);
  }

  cambiarTitulo(titulo) {
    this.title = titulo;
    this.events.publish('titulo:cambiado', titulo);
  }

  public usuarioChange() {
    this.observableUser.next(this.currentUser);
  }

  //Muestra los mensajes
  public showToast(message) {
    const toast = this.toastCtrl.create({
      message: message,
      duration: 4000,
      position: "middle"
    });
    toast.present();
  }

  //Muestra los mensajes con boton de cerrar
  public showToastConfirm(message) {
    const toast = this.toastCtrl.create({
      message: message,
      position: "middle",
      showCloseButton: true,
      closeButtonText: "cerrar"
    });
    toast.present();
  }

  public loguearUsuario(usuario: Usuario): any {
    let res: any = new Object();
    res.success = false;
    res.message = "No se encontró el usuario.";
    for (let i = 0; i < this.lstUsuarios.length; i++) {
      if (this.lstUsuarios[i].email == usuario.email || this.lstUsuarios[i].password == usuario.password) {
        localStorage.setItem("Usuario", JSON.stringify(this.lstUsuarios[i]));
        this.currentUser = this.lstUsuarios[i];
        this.usuarioChange();
        res.success = true;
      }

    }
    return res;
  }

}
